## ThreatFox CLI

> ThreatFox is a free platform from abuse.ch with the goal of sharing indicators of compromise (IOCs) associated with malware with the infosec community, AV vendors and threat intelligence providers.  

This is a command-line tool using the [ThreatFox api](https://threatfox.abuse.ch/api/). I like using CLI tools and wanted to practice my python, so I made this!  
Check out [abuse.ch](https://abuse.ch/) for other projects they've made. They're all pretty good. 

## Usage

### -I (--ip)
Takes an IP (IPv4 or IPv6). Checks the IP against the ThreatFox database. 

### -H (--hash)
Takes a file hash (MD5 and SHA256). Checks against the ThreatFox databse. 
